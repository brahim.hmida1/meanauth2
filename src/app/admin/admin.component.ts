import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { BlogpostService } from '../blogpost.service';
import { Blogpost } from '../models/blogpost';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  blogpost:Observable<Blogpost[]>;
  allBlogposts: Blogpost[];
  errorFromServer='';
  constructor(private blogpostService:BlogpostService,private authService:AuthService,private router:Router) { }

  ngOnInit(): void {
    if(!this.authService.isAuthenticated){
      this.router.navigate(['/auth'])

    }
     this.blogpostService.getBlogPosts().subscribe(data=>{
     this.refresh(data);
     this.blogpostService.handleBlogpostCreated().subscribe(data=>{
       console.log('Admin Component ',data);
     })
    });
  }
  deleteBlogposts(selectedOptions) {
    const ids = selectedOptions.map(so => so.value);
    if (ids.length === 1) {
      return this.blogpostService
        .deleteSingleBlogPost(ids[0])
        .subscribe(data => this.refresh(data), err => this.handleError(err));
    } else {
      return this.blogpostService
        .deleteBlogposts(ids)
        .subscribe(data => this.refresh(data), err => this.handleError(err));
    }
  }
  refresh(data) {
    console.log('data', data);
    this.blogpostService.getBlogPosts().subscribe(data => {
      this.allBlogposts = data;
    });
  }

  handleError(error) {
    this.errorFromServer= `Error ${error.status} - ${error.statusText}`;
    console.error(error);
  }
  logout(){
    this.authService.logout().subscribe(data=>{
      console.log(data);
      this.router.navigate(['/auth'])
    },err=>console.log(err));
  }

}
