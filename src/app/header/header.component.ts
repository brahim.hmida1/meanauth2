
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import {MatDialog} from '@angular/material/dialog';
import { DialogAuthComponent } from '../dialog-auth/dialog-auth.component';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() toggleSidebarForMe: EventEmitter<any> = new EventEmitter();

  constructor(private authService:AuthService,private router:Router,public dialog: MatDialog) {}

  ngOnInit(): void {}

  toggleSidebar() {
    this.toggleSidebarForMe.emit();
  }
  logout(){
    this.authService.logout().subscribe(data=>{
      console.log(data);
      this.router.navigate(['/auth'])
    },err=>console.log(err));
  }
  openDialog() {
   this.dialog.open(DialogAuthComponent);
  }
}
