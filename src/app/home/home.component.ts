import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BlogpostService } from '../blogpost.service';
import { Blogpost } from '../models/blogpost';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  blogpostList:Blogpost[];
  errMss:string;
    constructor(private blogpostService:BlogpostService) { }
  
    ngOnInit(): void {
      this.blogpostService.getBlogPosts().subscribe((blogposts)=>this.blogpostList=blogposts,err=>this.errMss=<any>this.errMss)  }
    }


