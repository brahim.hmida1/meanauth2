import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from '../admin/admin.component';
import { AuthComponent } from '../auth/auth.component';
import { BlogpostEditComponent } from '../blogpost-edit/blogpost-edit.component';
import { BlogpostListComponent } from '../blogpost-list/blogpost-list.component';
import { BlogpostComponent } from '../blogpost/blogpost.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HomeComponent } from '../home/home.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { RegisterComponent } from '../register/register.component';
import { TestComponent } from '../test/test.component';



  
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  {path:'auth',component:AuthComponent},
  {path:'test',component:TestComponent,outlet:"sidebar"},
  { path: 'blog',  component: BlogpostListComponent },
  { path: 'blog-posts/:id', component: BlogpostComponent },
  {path:'admin', component:AdminComponent},
  {path:'register', component:RegisterComponent},
  {path:'admin/blog-posts/:id',component:BlogpostEditComponent},
  {path: '**' ,component:PageNotFoundComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
