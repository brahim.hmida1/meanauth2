import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogpostListComponent } from '../blogpost-list/blogpost-list.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HomeComponent } from '../home/home.component';

 const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'blog',  component: BlogpostListComponent },

]

export const routingModule: ModuleWithProviders<any> = RouterModule.forRoot(routes);