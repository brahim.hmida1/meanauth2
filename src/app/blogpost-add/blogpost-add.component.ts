import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective } from '@angular/forms';
import { Subject } from 'rxjs';
import { BlogpostService } from '../blogpost.service';

@Component({
  selector: 'app-blogpost-add',
  templateUrl: './blogpost-add.component.html',
  styleUrls: ['./blogpost-add.component.css']
})
export class BlogpostAddComponent implements OnInit {

  creationForm:FormGroup;

  constructor(private fb: FormBuilder,private blogService:BlogpostService,private el:ElementRef) { }

  ngOnInit(): void {
    this.createForm();
  }
  createForm(){
    this.creationForm=this.fb.group({
      title:'',
      subTitle:'',
      content:'',
    })
  }


  upload(){
    let inputEl:HTMLInputElement=this.el.nativeElement.querySelector('#image');
    let fileCount: number=inputEl.files.length;
    console.log(fileCount+ " est le nombre de fichier a uploader");
    if(fileCount>0){
      let formData=new FormData();
      formData.append('image',inputEl.files.item(0));
      this.blogService.uploadImage(formData).subscribe((data)=>console.log(data),error=>console.error(error));
      
    }

  }


  createBlogpost(formGroupDirective:FormGroupDirective){
    if(this.creationForm.valid){
      console.log(this.creationForm.value);
      this.blogService.createBlogpost(this.creationForm.value).subscribe(data=>this.handleSuccess(data,formGroupDirective)),error=>this.handleError(error)

    }
 
  }
  handleSuccess(data,formGroupDirective){
console.log(data+" ok c bien passer");
this.creationForm.reset();
formGroupDirective.resetForm();
this.blogService.dispatchBlogpostCreated(data._id);
  }
  handleError(error){
console.log("error");
  }

}
