import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BlogpostService } from '../blogpost.service';
import { Blogpost } from '../models/blogpost';
import {Location} from '@angular/common';
@Component({
  selector: 'app-blogpost',
  templateUrl: './blogpost.component.html',
  styleUrls: ['./blogpost.component.css']
})
export class BlogpostComponent implements OnInit {
  blogpost: Blogpost;
  blogpost$: Observable<Blogpost>;
  imagePath=environment.imagePath;


  constructor(private activatedRoute: ActivatedRoute, private blogpostService: BlogpostService,private _location: Location) { }

  ngOnInit() {
     const id = this.activatedRoute.snapshot.paramMap.get('id');
     this.blogpost$ = this.blogpostService.getBlogPostById(id);

  }
  backClicked() {
    this._location.back();
  }
}
